package main

import (
	"database/sql"
	"log"
)

/*

source pand3d fields.

gid
identificatie
bouwjaar
geovlak
gemeentecode

-- not so usefull.
aanduidingrecordinactief
officieel
inonderzoek
documentnummer
documentdatum

*/

func (p *pand) stats(product string) *sjv_stats {
	return &sjv_stats{}
}

type sjv_stats struct {
	yearcount int64
	std       int64
	avge      int64
	trend     int64
	trend_p   int64
}

type pand struct {
	gid       int64
	pand_id   string
	geometrie string
	postcode  []string

	gemeentecode string
	bouwjaar     int64

	kv map[int64]PropertyMap
}

type pand_postcode struct {
	pand_id  string
	postcode string
}

/*
func (i wegdeelOccupancyResult) Row() []string {
	return []string{
		strconv.Itoa(int(i.ID)),
		i.BgtID,
		i.BgtFunctie,
		i.Geometrie,
		i.Buurt,
		strconv.Itoa(int(i.Vakken)),
		strconv.Itoa(int(i.FiscaleVakken)),
		strconv.Itoa(int(i.ScanCount)),
		strconv.Itoa(int(i.AvgOccupany)),
		strconv.Itoa(int(i.MinOccupany)),
		strconv.Itoa(int(i.MaxOccupany)),
		strconv.Itoa(int(i.StdOccupany)),
		strconv.Itoa(int(i.BuckerCount)),
		i.Geometrie,
	}
}
*/

/*
id               int6
bgt_id
bgt_functie
geometrie
vakken
fiscale_vakken
scan_count
*/

func preparePandenSelect(db *sql.DB) *sql.Stmt {

	query := `select
	gid,
	identificatie,
	gemeentecode,
	st_asewkt(geovlak) as geometrie
	from "3dbag".energiepanden
	where gid >= $1 and gid < $2
	order by gid
	`

	stmt, err := db.Prepare(query)

	if err != nil {
		log.Fatal(err)
	}

	return stmt
}

func preparePandPostcode(db *sql.DB) *sql.Stmt {

	query := `select
	identificatie,
	postcode
	from public.pand_postcode
	`

	stmt, err := db.Prepare(query)

	if err != nil {
		log.Fatal(err)
	}

	return stmt
}

func channelPostcodes(stmt *sql.Stmt, items chan *pand_postcode) {

	wgSql.Add(1)

	rows, err := stmt.Query()

	if err != nil {
		log.Fatal(err)
	}

	var pand_id sql.NullString
	var postcode sql.NullString

	for rows.Next() {
		if err := rows.Scan(
			&pand_id,
			&postcode,
		); err != nil {
			log.Fatal(err)
		}

		p := &pand_postcode{
			pand_id:  convertSqlNullString(pand_id),
			postcode: convertSqlNullString(postcode),
		}
		items <- p
	}

	rerr := rows.Close()

	if rerr != nil {
		log.Fatal(err)
	}

	// Rows.Err will report the last error encountered by Rows.Scan.
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	wgSql.Done()

}

func channelPandenFromDB(stmt *sql.Stmt, items chan *pand, begin int, end int) {

	rows, err := stmt.Query(begin, end)

	if err != nil {
		log.Fatal(err)
	}

	var gid int64
	var pand_id sql.NullString
	var geometrie sql.NullString
	var gemeente sql.NullString
	var bouwjaar sql.NullInt64

	for rows.Next() {
		if err := rows.Scan(
			&gid,
			&pand_id,
			&geometrie,
			&gemeente,
		); err != nil {
			log.Fatal(err)
		}

		p := &pand{
			gid:          gid,
			pand_id:      convertSqlNullString(pand_id),
			geometrie:    convertSqlNullString(geometrie),
			bouwjaar:     convertSqlNullInt(bouwjaar),
			gemeentecode: convertSqlNullString(gemeente),
		}

		items <- p
	}

	rerr := rows.Close()

	if rerr != nil {
		log.Fatal(err)
	}

	// Rows.Err will report the last error encountered by Rows.Scan.
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	wgSql.Done()
}
