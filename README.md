# gopandcalculator

Load buildings in the Netherlands in memory (~12.100.000 objects)  from a PostgreSQL database in memory
and do calculations on them.

Written in golang.  Takes 43 seconds to load the data in memory and do a calculation.

Requires a fully prepared database with the buildings of the Netherlands
available for download at commondatafacotory.nl/docs or create it yourself.


![relations](/docs/graph.png)
