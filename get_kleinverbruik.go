package main

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"errors"
	//"fmt"
	"log"
)

/*

verbruik pand tables columns

id  -> / building / pand_id
data -> json field.


*/

/*
type elkdata struct {
	Kwh               float64
	aansluitingen     int
	leveringsrichting int
}

type gasdata struct {
	m3                float64
	aansluitingen     int
	leveringsrichting int
}

type kvdata struct {
	idr         int // group.
	elk         elkdata
	gas         gasdata
	pandencound int
}
*/

type kvpand struct {
	pand_id string
	data    PropertyMap
}

func prepareVerbruikPand(db *sql.DB, year int) *sql.Stmt {

	query := `select
	identificatie,
        postcode
	from public.pand_postcode
	`

	//query = fmt.Sprintf(query)

	stmt, err := db.Prepare(query)

	if err != nil {
		log.Fatal(err)
	}

	return stmt
}

type PropertyMap map[string]interface{}

func (p *PropertyMap) Value() (driver.Value, error) {
	j, err := json.Marshal(p)
	return j, err
}

func (p PropertyMap) Scan(src interface{}) error {
	source, ok := src.([]byte)

	if !ok {
		return errors.New("type assertion. ([]byte) failed.")
	}

	// var i interface{}
	err := json.Unmarshal(source, &p)

	if err != nil {
		return err
	}

	//	*p, ok = i.(map[string]interface{})
	//  if !ok {
	//	return errors.New("type assertion .map[string]interface{}")
	//}

	return nil
}

func channelKleinverbruik(stmt *sql.Stmt, items chan *kvpand) {

	wgKV.Add(1)

	rows, err := stmt.Query()

	if err != nil {
		log.Fatal(err)
	}

	for rows.Next() {
		p := new(kvpand)
		if err := rows.Scan(
			&p.pand_id,
			&p.data,
		); err != nil {
			log.Fatal(err)
		}
		items <- p
	}

	rerr := rows.Close()

	if rerr != nil {
		log.Fatal(err)
	}

	// Rows.Err will report the last error encountered by Rows.Scan.
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	wgSql.Done()
	wgKV.Done()
}
