package main

import (
	"database/sql"
	"fmt"
	"log"
	"runtime"
	"sync"
	"time"
)

// globals
var wgKV sync.WaitGroup
var wgSql sync.WaitGroup
var wgAppend sync.WaitGroup

// var mapRows int

var Allpanden map[string]*pand

var KVPanden map[string]*kvpand

// var panden map[string]*pand
var pCounter int

func init() {
	// run settings
	SETTINGS.SetInt("sqlworkers", 4, "Specify mount of sql tasks")
	SETTINGS.SetInt("maxlimit", 19000000, "max primary key id")
	SETTINGS.SetInt("bucketsize", 500000, "max primary key id")

	// postgres settings
	SETTINGS.Set("dbhost", "database", "Specify Elastic search Host")
	SETTINGS.Set("dbpwsd", "insecure", "Set Database Password")
	SETTINGS.Set("dbname", "cdf", "Set database name")
	SETTINGS.Set("dbuser", "cdf", "Set database user")
	SETTINGS.SetInt("dbport", 5432, "Specify database port")

	SETTINGS.Parse()

	Allpanden = make(map[string]*pand, 800)
	KVPanden = make(map[string]*kvpand, 800)
	pCounter = 0
}

func prepareBuildings(db *sql.DB) {

	chItems := make(chan *pand, 10000)

	go buildBuildingsMap(chItems)
	stmt := preparePandenSelect(db)
	defer stmt.Close()

	bucketsSize := 500000

	for begin := 0; begin <= 19000000; begin += bucketsSize {
		end := begin + bucketsSize
		wgSql.Add(1)
		go channelPandenFromDB(stmt, chItems, begin, end)
	}

	go printStatus(chItems)

	wgSql.Wait()
	close(chItems)
	wgAppend.Wait()

}

/*
func preparekv(db *sql.DB) {

	chItems := make(chan *kvpand, 10000)
	stmt := prepareVerbruikPand(db, 2020)

	go addKVtoBuildingsMap(chItems, 2020)
	go channelKleinverbruik(stmt, chItems)
	// go loadKV(stmt, chItems)
	wgSql.Wait()
	close(chItems)
	wgAppend.Wait()
	//go build
}
*/

func loadPostcodes(db *sql.DB) {
	chItems := make(chan *pand_postcode, 10000)
	go addPostcodestoBuildingsMap(chItems)
	stmt := preparePandPostcode(db)
	go channelPostcodes(stmt, chItems)
	wgSql.Wait()
	close(chItems)
	wgAppend.Wait()
}

func main() {

	db, err := dbConnect(ConnectStr())

	if err != nil {
		log.Fatal(err)
	}

	db.SetMaxOpenConns(6)

	go runPrintMem()

	// preparekv(db)
	prepareBuildings(db)
	loadPostcodes(db)
	log.Printf("STATUS: panden loaded: %-10d", pCounter)
}

//bToMB Bytes To MegaBytes
func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}

func printMemUsage() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	// For info on each, see: https://golang.org/pkg/runtime/#MemStats
	fmt.Printf("Alloc = %v MiB", bToMb(m.Alloc))
	fmt.Printf("\tTotalAlloc = %v MiB", bToMb(m.TotalAlloc))
	fmt.Printf("\tSys = %v MiB", bToMb(m.Sys))
	fmt.Printf("\tNumGC = %v\n", m.NumGC)
}

func runPrintMem() {
	for {
		printMemUsage()
		time.Sleep(8 * time.Second)
	}
}

func buildBuildingsMap(chItems chan *pand) {
	wgAppend.Add(1)
	for p := range chItems {
		pCounter++
		Allpanden[p.pand_id] = p
	}
	wgAppend.Done()
}

func addPostcodestoBuildingsMap(chItems chan *pand_postcode) {
	wgAppend.Add(1)
	for pp := range chItems {
		if p, ok := Allpanden[pp.pand_id]; ok {
			p.postcode = append(p.postcode, pp.postcode)
		}
	}
	wgAppend.Done()
}

func addKVtoBuildingsMap(chItems chan *kvpand, year int64) {
	wgAppend.Add(1)

	for pp := range chItems {
		if p, ok := Allpanden[pp.pand_id]; ok {
			p.kv[year] = pp.data
		} else {

		}
	}
}

func printStatus(chItems chan *pand) {
	i := 1
	delta := 5
	duration := 0
	speed := 0

	for {
		time.Sleep(time.Duration(delta) * time.Second)
		duration = i * delta
		speed = pCounter / duration
		log.Printf("STATUS: rows:%-10d  %-10d rows/sec  buffer: %d", pCounter, speed, len(chItems))
		i++
	}
}
